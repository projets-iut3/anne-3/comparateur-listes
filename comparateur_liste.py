import heapq
import numpy as np
import time 
import matplotlib.pyplot as plt
import copy


def element_commun1(l1, l2):

    while len(l1) > 0 and len(l2) > 0:  # On itère tant qu'aucune des deux listes n'est vide 
        m1, m2 = min(l1), min(l2)       # On obtient les valeurs minimum des deux listes
        if (m1 == m2):                      
            return m1
            # Si elles sont égales, on renvoie et on finit l'execution

        if (m1 < m2):
            l1.remove(m1)
            # Si m1 est plus petit que m2 on enlève m1 de sa liste
        
        if (m1 > m2):
            l2.remove(m2)
            # Si m2 est plus petit que m1 on enlève m2 de sa liste

    # Si jamais on ne trouve aucune correspondance on renvoie NaN
    return float('nan')

def element_commun2(l1, l2):
    heapq.heapify(l1)
    heapq.heapify(l2)

    while len(l1) > 0 and len(l2) > 0:  # On itère tant qu'aucune des deux listes n'est vide 
        m1, m2 = heapq.nsmallest(1, l1)[0], heapq.nsmallest(1, l2)[0] # On obtient les valeurs minimum des deux listes
        if (m1 == m2):
            return m1
            # Si elles sont égales, on renvoie et on finit l'execution

        if (m1 < m2):
            heapq.heappop(l1)
            # Si m1 est plus petit que m2 on enlève m1 de sa liste

        if (m1 > m2):
            heapq.heappop(l2)
            # Si m2 est plus petit que m1 on enlève m2 de sa liste

    # Si jamais on ne trouve aucune correspondance on renvoie NaN
    return float('nan')


def tests1(n, Nmax):
    l1 = np.random.randint(Nmax,size=n).tolist()
    l2 = np.random.randint(Nmax,size=n).tolist()
    # après avoir généré deux listes on execute la fonction et renvoie son résultat
    return element_commun1(l1, l2)


def perf():
    x = []
    t1 = []
    t2 = []
    for i in range(1, 10000, 50):
        x.append(i)
        
        # après avoir généré deux listes on les clone
        l1 = np.random.randint(100,size=i).tolist()
        l2 = np.random.randint(100,size=i).tolist()
        l1bis = copy.deepcopy(l1)
        l2bis = copy.deepcopy(l2)

        # On exécute chacun des algo et on ajoute le temps d'éxécution à une liste dédiée
        temps = time.time()
        element_commun1(l1, l2)
        t1.append(time.time() - temps)
        temps = time.time()
        element_commun2(l1bis, l2bis)
        t2.append(time.time() - temps)

    # On fait ensuite un graphique !
    plt.figure()
    plt.scatter(x, t1, color='blue', label="Sans tas", marker='.')
    plt.scatter(x, t2, color='red', label="Avec structure de tas", marker='.')
    plt.xlabel("Taille des listes")
    plt.ylabel("Temps d'execution en secondes")
    plt.legend()
    plt.title("T = f(données) pour les tas")

    plt.savefig('plot-perf-heap.jpg')
    # plt.show()

perf()

# print(element_commun1([2, 4, 5, 7, 8], [1, 3, 4, 7, 8]))
